# xivo-python-wsaccel-packaging

Debian packaging for [wsaccel](https://github.com/methane/wsaccel) used in XiVO.

## Upgrading

To upgrade wsaccel:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes

To build locally:

```
debian/rules get-orig-source
tar xf ../xivo-python-wsaccel-packaging_*.orig.tar.gz
mv wsaccel-*/* .
rmdir wsaccel-*
dpkg-buildpackage
```
